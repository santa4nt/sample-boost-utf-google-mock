#ifndef BAR_H_DEFINED
#define BAR_H_DEFINED

#include "IBar.h"

class CBar : public IBar
{
    std::string DoBar();
};

#endif
