#ifndef FOO_H_DEFINED
#define FOO_H_DEFINED

#include "IBar.h"
#include <memory>
#include <string>

class CFoo
{
public:

    CFoo(std::shared_ptr<IBar> pBar);
    ~CFoo();

    std::string DoFoo();

private:

    std::shared_ptr<IBar> m_pBar;
};

#endif
