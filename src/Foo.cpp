#include "Foo.h"

CFoo::CFoo(std::shared_ptr<IBar> pBar)
    : m_pBar(pBar)
{
}

CFoo::~CFoo()
{
}

std::string CFoo::DoFoo()
{
    return std::string("Foo ") + m_pBar->DoBar();
}

