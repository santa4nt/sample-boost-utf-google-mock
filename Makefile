# Assuming Boost's UTF (Unit Test Framework) is already installed on the system via
# a package manager, and Google Mock Framework is downloaded [1] and unzipped locally
# on a user path ...
#
# [1] https://code.google.com/p/googlemock/downloads/list

#GMOCK_DIR = /path/to/gmock		# replace with path to a Google Mock checkout directory
GMOCK_DIR = /home/santa/opt/gmock-1.7.0
GTEST_DIR = $(GMOCK_DIR)/gtest

VPATH = inc:src:test
BDIR = build
LDIR = library

CXX = g++
CXXFLAGS = -Wall -std=c++11
INCLUDES = -Iinc -Isrc
GMOCK_INCLUDES = -I$(GMOCK_DIR)/include -I$(GTEST_DIR)/include
DEFINES =
LIBS = -lboost_unit_test_framework -lpthread -lgmock
LDFLAGS = -L$(LDIR)

OBJS = $(addprefix $(BDIR)/,\
	   		Foo.o \
			Bar.o \
			FooTest.o \
			main.o \
		)

GMOCK_MAKE_DIR = $(GMOCK_DIR)/make
LIBGMOCK = $(GMOCK_MAKE_DIR)/gmock.a
LIBGMOCK_CP = $(LDIR)/libgmock.a

TARGET = FooTest


all: target

# building the target depends on static gmock library being present/built
target: $(LIBGMOCK_CP) $(OBJS)
	$(CXX) -o $(TARGET) $(LDFLAGS) $(OBJS) $(LIBS)


# rules for building gmock library
$(LDIR):
	mkdir -p $(LDIR)

$(LIBGMOCK_CP): | $(LDIR)

$(LIBGMOCK):
	$(shell cd ${GMOCK_MAKE_DIR} ; make gmock.a)

$(LIBGMOCK_CP): $(LIBGMOCK)
	cp $(LIBGMOCK) $(LIBGMOCK_CP)


# rules for building our target objects
$(BDIR):
	mkdir -p $(BDIR)

$(OBJS): | $(BDIR)

$(BDIR)/%.o: %.cpp
	$(CXX) -c -o $@ $< $(DEFINES) $(CXXFLAGS) $(INCLUDES) $(GMOCK_INCLUDES)


clean:
	rm -rf $(BDIR)
	rm -rf $(LDIR)
	rm -f $(TARGET)

.PHONY: clean

