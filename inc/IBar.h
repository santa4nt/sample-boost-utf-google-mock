#ifndef IBAR_H_DEFINED
#define IBAR_H_DEFINED

#include <string>

class IBar
{
public:
    virtual std::string DoBar() = 0;
};

#endif
