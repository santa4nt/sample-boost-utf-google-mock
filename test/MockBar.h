#ifndef MOCKBAR_H_DEFINED
#define MOCKBAR_H_DEFINED

#include "IBar.h"
#include "gmock/gmock.h"

class CMockBar : public IBar
{
public:
    MOCK_METHOD0(DoBar, std::string());
};

#endif
