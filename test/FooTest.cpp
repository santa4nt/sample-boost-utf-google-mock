#include "Foo.h"
#include "MockBar.h"
#include <boost/test/unit_test.hpp>

using namespace ::testing;


struct FooTestSuiteFixture
{
    FooTestSuiteFixture()
    {
        m_pBar = std::make_shared<CMockBar>();
        m_pFoo = std::move(
                    std::unique_ptr<CFoo>(
                        new CFoo(m_pBar)));
    }

    ~FooTestSuiteFixture()
    {
    }

    std::unique_ptr<CFoo> m_pFoo;
    std::shared_ptr<CMockBar> m_pBar;
};


BOOST_FIXTURE_TEST_SUITE( FooTestSuite, FooTestSuiteFixture )

BOOST_AUTO_TEST_CASE( Foo_DoFoo_ReturnsFooBar )
{
    std::string szMockBar = std::string("MockBar");

    EXPECT_CALL( *m_pBar, DoBar() )
        .Times(1)
        .WillOnce( Return( szMockBar ) );

    std::string szRet = m_pFoo->DoFoo();
    BOOST_REQUIRE_EQUAL(szRet, std::string("Foo ") + szMockBar);
}

BOOST_AUTO_TEST_SUITE_END() // FooTestSuiteFixture

