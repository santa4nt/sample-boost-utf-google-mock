#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE       FooTest
#include <boost/test/unit_test.hpp>
#include "gmock/gmock.h"

using namespace boost::unit_test;

test_suite*
init_unit_test_suite(int argc, char* argv[])
{
    ::testing::GTEST_FLAG(throw_on_failure) = true;
    ::testing::InitGoogleMock(&argc, argv);

    framework::master_test_suite().p_name.value = "Foo Test Suite";

    return nullptr;
}

